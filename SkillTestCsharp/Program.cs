﻿using System;

namespace SkillTestCsharp
{
    class Program
    {
        static void Main(string[] args)
        {
            // Console.WriteLine("Hello World!");
            Mymenu();
        }
        static string Mymenu()
        {
            Console.Clear();
            Console.WriteLine("Skill Test Interview");
            Console.WriteLine("");
            Console.WriteLine("1. Bubble Sort");
            Console.WriteLine("2. Quick Sort");
            Console.WriteLine("3. Exit");
            Console.WriteLine("Type Sorting option:");
            string myoptions;
            myoptions = Console.ReadLine();
            switch (myoptions)
            {
                case "1":
                    BubbleSort();
                    break;
                case "2":
                    QuickSort();
                    break;
                case "3":
                    Exit();
                    break;
                default:
                    Console.WriteLine("None of the Option you choose, Please select in 1, 2, or 3 again for the option.\n \n");
                    Console.WriteLine("Please Enter key to continue... ");
                    break;





            }
            Console.WriteLine(myoptions);
            Console.ReadLine();
            return Mymenu();

        }
        static void BubbleSort()
        {
            char temp;
            string myStr;
            Console.Write("Bubble Sort \n");
            Console.Write("Enter String : ");
            myStr = Convert.ToString(Console.ReadLine());
            string str = myStr.ToLower();
            char[] charstr = str.ToCharArray();

            for (int i = 1; i < charstr.Length; i++)
            {
                for (int j = 0; j < charstr.Length - 1; j++)
                {
                    if (charstr[j] > charstr[j + 1])
                    {
                        temp = charstr[j];
                        charstr[j] = charstr[j + 1];
                        charstr[j + 1] = temp;

                    }

                }
            }

            Console.WriteLine(charstr);
            Console.ReadLine();

        }
        static void QuickSort()
        {
            char temp1;
            string myStr1;
            Console.Write("Quick Sort \n");
            Console.Write("Enter String : ");
            myStr1 = Convert.ToString(Console.ReadLine());
            string str1 = myStr1.ToLower();
            char[] charstr1 = str1.ToCharArray();

            for (int i = 1; i < charstr1.Length; i++)
            {
                for (int j = 0; j < charstr1.Length - 1; j++)
                {
                    if (charstr1[j] > charstr1[j + 1])
                    {
                        temp1 = charstr1[j];
                        charstr1[j] = charstr1[j + 1];
                        charstr1[j + 1] = temp1;
                    }
                }
            }
            Console.WriteLine(charstr1);
            Console.ReadLine();

        }
        static void Exit()
        {
            Console.WriteLine("Are you sure you would like to exit this program");
            Console.WriteLine("Press the Enter key to close this program");
            Console.ReadLine();
            System.Environment.Exit(1);

        }

    }

}
    


